use ff::PrimeField;

/// A dense matrix. Apple have said t and s are going to be on the order of 30 so I feel
/// you can probably always get away with a dense representation
pub struct Matrix<PF>
where
    PF: PrimeField,
{
    rows: usize,
    cols: usize,
    vals: Vec<Vec<PF>>,
}

impl<PF> Matrix<PF>
where
    PF: PrimeField,
{
    pub fn new(rows: usize, cols: usize) -> Matrix<PF> {
        let mut vals = vec![];
        for _i in 0..rows {
            let mut row = vec![];
            for _j in 0..cols {
                row.push(PF::zero())
            }
            vals.push(row)
        }
        Matrix { rows, cols, vals }
    }

    pub fn rows(&self) -> usize {
        self.rows
    }

    pub fn cols(&self) -> usize {
        self.cols
    }

    pub fn update(&mut self, row: usize, col: usize, val: PF) {
        self.vals[row][col] = val
    }

    pub fn at(&self, row: usize, col: usize) -> PF {
        self.vals[row][col]
    }

    pub fn transpose(&self) -> Matrix<PF> {
        let mut m = Matrix::new(self.cols, self.rows);
        for i in 0..self.cols {
            for j in 0..self.rows {
                m.update(i, j, self.vals[j][i]);
            }
        }
        m
    }

    pub fn swap_rows(&mut self, a: usize, b: usize) {
        let tmp = self.vals[a].clone();
        self.vals[a] = self.vals[b].clone();
        self.vals[b] = tmp;
    }
}
